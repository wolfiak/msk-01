package com.example.pacio.msk_02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<String> stringi;
    private int index;
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        stringi=new ArrayList<>();
        index=0;
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,stringi);
        ListView listView=(ListView) findViewById(R.id.lista);
        listView.setAdapter(adapter);


        Button przycisk= (Button) findViewById(R.id.button);
        przycisk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stringi.add("item"+index);
                index++;
                adapter.notifyDataSetChanged();
            }
        });
        
    }
}
